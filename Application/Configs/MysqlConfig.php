<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 foldmethod=marker: */
/**
 * @package
 * @file                 $RCSfile: MySQLConfig.php,v $
 * @version              $Revision: 1.0 $
 * @modifiedby           $Author: handaoliang $
 * @lastmodified         $Date: 2013/11/30 20:51:09 $
 * @copyright            Copyright (c) 2013, Comnovo Inc.
**/
/**
 * MySQL集群配置文件。
**/
return array (
    "master" => array (
        "db_host"         =>"localhost",
        "db_port"         =>3306,
        "db_user"         =>"root",
        "db_password"     =>"",
        "db_name"         =>"novophp_com",
        "db_table_pre"    =>"novophp_",
        "db_charset"      =>"utf8",
        "db_type"         =>"mysql",
        "db_debug"        =>true,
    ),

    "slave" => array (
        "db_host"         =>"localhost",
        "db_port"         =>3306,
        "db_user"         =>"root",
        "db_password"     =>"",
        "db_name"         =>"novophp_com",
        "db_table_pre"    =>"novophp_",
        "db_charset"      =>"utf8",
        "db_type"         =>"mysql",
        "db_debug"        =>true,
    ),
);
